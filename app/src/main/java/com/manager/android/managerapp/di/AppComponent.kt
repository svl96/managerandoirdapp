package com.manager.android.managerapp.di

import com.manager.android.managerapp.ui.ClientInfoViewModel
import com.manager.android.managerapp.ui.ClientsViewModel
import com.manager.android.managerapp.ui.MapsViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules =  [AppModule::class] )
interface AppComponent {

    fun inject(clientsViewModel: ClientsViewModel)

    fun inject(clientInfoViewModel: ClientInfoViewModel)

    fun inject(mapsViewModel: MapsViewModel)

}