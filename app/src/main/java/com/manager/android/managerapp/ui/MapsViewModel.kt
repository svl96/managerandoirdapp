package com.manager.android.managerapp.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.manager.android.managerapp.App
import com.manager.android.managerapp.domain.LoadClientsUseCase
import com.manager.android.managerapp.domain.route.RouteResponse
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class MapsViewModel(app: Application) : AndroidViewModel(app) {

    companion object {
        const val TAG = "MapsViewModel"
    }

    init {
        (app as App).getAppComponent().inject(this)
    }

    var loadClientsUseCase : LoadClientsUseCase? = null
    @Inject set

    private val mShowError : MutableLiveData<Boolean> = MutableLiveData()
    private val mRouteInfoLiveData : MutableLiveData<RouteResponse> = MutableLiveData()

    private val mCompositeDisposable = CompositeDisposable()


    fun loadRoute(position: String?, destination: String?) {
        if (position.isNullOrEmpty() || destination.isNullOrEmpty()) {
            return
        }

        if (loadClientsUseCase == null) {
            mShowError.postValue(true)
        }
        Log.d(TAG, "LoadRoute")
        val disposable: Disposable = loadClientsUseCase!!.getRoute(position!!, destination!!)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        { response ->
                            Log.d(TAG, "response")
                            if (response.isSuccessful) {
                                val route: RouteResponse? = response.body()
                                if (route != null) {
                                    if (route.getRoutes().isNotEmpty()) {
                                        mRouteInfoLiveData.postValue(route)
                                        mShowError.postValue(false)
                                    } else {
                                        mShowError.postValue(true)
                                    }
                                }
                            }
                        },
                        {
                            throwable -> Log.e(TAG, "loadRoute", throwable)
                            mShowError.postValue(true)
                        }
                )

        mCompositeDisposable.add(disposable)
    }

    fun getRoute() : LiveData<RouteResponse> {
        return mRouteInfoLiveData
    }

    fun getShowError() : LiveData<Boolean> {
        return mShowError
    }


    override fun onCleared() {
        mCompositeDisposable.dispose()
    }
}