package com.manager.android.managerapp.domain.route

import com.google.gson.annotations.SerializedName

class RouteResponse(routes: List<Route>) {

    @SerializedName("routes")
    private val mRoutes : List<Route> = routes

    fun getRoutes() : List<Route> {
        return mRoutes
    }

    class Route(overviewPolyline: OverviewPolyline) {

        @SerializedName("overview_polyline")
        private val mOverviewPolyline: OverviewPolyline = overviewPolyline

        fun getOverviewPolyline(): OverviewPolyline {
            return mOverviewPolyline
        }
    }

    class OverviewPolyline(points: String) {

        @SerializedName("points")
        private val mPoints: String = points

        fun getPoints() :String {
            return mPoints
        }
    }
}