package com.manager.android.managerapp.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.manager.android.managerapp.domain.Client

@Database(version = ClientsDatabase.DATABASE_VERSION,
        entities = [Client::class])
abstract class ClientsDatabase : RoomDatabase() {
    companion object {
        const val DATABASE_NAME = "clients.db"
        const val DATABASE_VERSION = 1
    }

    abstract fun clientsDao(): ClientsDao


}