package com.manager.android.managerapp

import android.app.Application
import android.util.Log
import com.manager.android.managerapp.di.AppComponent
import com.manager.android.managerapp.di.AppModule
import com.manager.android.managerapp.di.DaggerAppComponent


class App : Application() {

    private var mAppComponent : AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        Log.d("App", "OnCreate")
        mAppComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    fun getAppComponent() : AppComponent {
        return mAppComponent!!
    }
}