package com.manager.android.managerapp.domain

import com.manager.android.managerapp.domain.route.RouteResponse
import io.reactivex.Single
import retrofit2.Response


interface GoogleServices {

    fun getRoute(position: String, destination: String) : Single<Response<RouteResponse>>
}