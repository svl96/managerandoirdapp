package com.manager.android.managerapp.data.local

import com.manager.android.managerapp.domain.Client
import com.manager.android.managerapp.domain.LocalRepository
import io.reactivex.Flowable

class LocalRepositoryImpl(private val clientsDao: ClientsDao) : LocalRepository {

    override fun insertOrUpdate(clients: List<Client>) {
        clientsDao.insertOrUpdate(clients)
    }

    override fun deleteClient(client: Client) {
        clientsDao.deleteClient(client)
    }

    override fun getClientList(): Flowable<List<Client>> {
        return clientsDao.getClientList()
    }

    override fun getClient(uid: String): Flowable<List<Client>> {
        return clientsDao.getClient(uid)
    }
}