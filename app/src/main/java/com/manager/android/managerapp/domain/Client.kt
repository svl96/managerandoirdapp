package com.manager.android.managerapp.domain

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName
import com.manager.android.managerapp.data.local.ClientsDao
import org.jetbrains.annotations.NotNull


@Entity(tableName = ClientsDao.TABLE_NAME,
        primaryKeys = [ClientsDao.Columns.ID])
class Client(id: String, name: String, address: String, phone: String, location: String) {

    @SerializedName("uid")
    @ColumnInfo(name = ClientsDao.Columns.ID)
    @NotNull
    private val mId : String = id

    @SerializedName("name")
    @ColumnInfo(name = ClientsDao.Columns.NAME)
    @NotNull
    private val mName : String = name

    @SerializedName("address")
    @ColumnInfo(name = ClientsDao.Columns.ADDRESS)
    @NotNull
    private val mAddress : String = address

    @SerializedName("phone")
    @ColumnInfo(name = ClientsDao.Columns.PHONE)
    @NotNull
    private val mPhone : String = phone

    @SerializedName("location")
    @ColumnInfo(name = ClientsDao.Columns.LOCATION)
    @NotNull
    private val mLocation : String = location


    fun getId() : String {
        return mId
    }

    fun getName() : String {
        return mName
    }

    fun getAddress() : String {
        return mAddress
    }

    fun getPhone() : String {
        return mPhone
    }

    fun getLocation() : String {
        return mLocation
    }
}