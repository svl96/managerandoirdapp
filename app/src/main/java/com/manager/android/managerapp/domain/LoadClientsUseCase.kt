package com.manager.android.managerapp.domain

import com.manager.android.managerapp.domain.route.RouteResponse
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class LoadClientsUseCase(private val localRepository: LocalRepository,
                         private val remoteRepository: RemoteRepository,
                         private val googleServices: GoogleServices) {

    // region Local Repository

    fun getClients() : Flowable<List<Client>> {
        val data = localRepository.getClientList()
        updateClients()
        return data
    }

    fun getClient(clientId: String) : Flowable<List<Client>> {
        return localRepository.getClient(clientId)
    }

    fun insertOrUpdateClients(clients: List<Client>) {
        Schedulers.io().createWorker().schedule {
            localRepository.insertOrUpdate(clients)
        }
    }

    fun deleteClient(client: Client) {
        Schedulers.io().createWorker().schedule {
            localRepository.deleteClient(client)
        }
    }

    // endregion

    fun updateClients() {

    }

    fun getRoute(position: String, destination: String) : Single<Response<RouteResponse>> {
        return googleServices.getRoute(position, destination)
    }
}