package com.manager.android.managerapp.data.remote

import com.manager.android.managerapp.domain.route.RouteResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RouteService {
    companion object {
        const val BASE_URL : String ="https://maps.googleapis.com"
    }

    @GET("maps/api/directions/json")
    fun getRoute(
            @Query(value = "origin", encoded=false) position: String,
            @Query(value = "destination", encoded=false) destination: String,
            @Query("sensor") sensor: Boolean,
            @Query("language") language: String) : Single<Response<RouteResponse>>

}