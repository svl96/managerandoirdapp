package com.manager.android.managerapp.domain

import io.reactivex.Flowable

interface LocalRepository {

    fun getClientList() : Flowable<List<Client>>

    fun getClient(uid: String) : Flowable<List<Client>>

    fun insertOrUpdate(clients: List<Client>)

    fun deleteClient(client: Client)
}