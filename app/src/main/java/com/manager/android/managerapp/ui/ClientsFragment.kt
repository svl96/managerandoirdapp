package com.manager.android.managerapp.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.manager.android.managerapp.R


class ClientsFragment : Fragment() {
    companion object {
        const val TAG = "ClientsFragment"

        @JvmStatic
        fun getInstance() : ClientsFragment {
            return ClientsFragment()
        }
    }

    private var swipeRefreshLayout : SwipeRefreshLayout? = null
    private var recyclerView : RecyclerView? = null
    private var clientsRecyclerAdapter: ClientsRecyclerAdapter? = null

    private var addButton: Button? = null

    private var clientsViewModel : ClientsViewModel? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_clients, container, false)!!
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        addButton = view?.findViewById(R.id.add_button)
//
//        addButton?.setOnClickListener {
//            addClients()
//        }

        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh)
        swipeRefreshLayout?.setOnRefreshListener {
            onUpdateClients()
        }

        setupRecyclerView(view)
    }

    private fun addClients() {
        clientsViewModel?.addClients()
    }

    private fun setupRecyclerView(view: View?) {
        recyclerView = view?.findViewById(R.id.recycler_clients)
        recyclerView?.layoutManager = LinearLayoutManager(context)

        clientsRecyclerAdapter = ClientsRecyclerAdapter(object : ClientsRecyclerAdapter.Callback {
            override fun onItemClick(clientId: String) {
                openClientInfoFragment(clientId)
            }

        })

        recyclerView?.adapter = clientsRecyclerAdapter
    }

    private fun openClientInfoFragment(clientId: String) {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container,
                        ClientInfoFragment.getInstance(clientId),
                        ClientInfoFragment.TAG
                        )
                .addToBackStack(null)
                .commit()
    }

    private fun onUpdateClients() {
        clientsViewModel?.onUpdateClients()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d(TAG, "OnActivityCreated")
        clientsViewModel = ViewModelProviders.of(this).get(ClientsViewModel::class.java)

        subscribeUI()
    }

    private fun subscribeUI() {
        clientsViewModel?.getClients()?.observe(this, Observer {
            clients ->
            swipeRefreshLayout?.isRefreshing = false
            clientsRecyclerAdapter?.setClients(clients)
        })

        clientsViewModel?.getShowError()?.observe(this, Observer {
            isError ->
            if (isError != null && isError) {
                Toast.makeText(context, "Loading Error", Toast.LENGTH_SHORT).show()
            }
        })

        clientsViewModel?.getShowLoading()?.observe(this, Observer {
            isShowLoading ->
            swipeRefreshLayout?.isRefreshing = isShowLoading != null && isShowLoading
        })
    }

}