package com.manager.android.managerapp.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.manager.android.managerapp.App
import com.manager.android.managerapp.domain.Client
import com.manager.android.managerapp.domain.LoadClientsUseCase
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.*
import javax.inject.Inject


class ClientsViewModel(app: Application) : AndroidViewModel(app) {
    companion object {
        const val TAG = "ClientsViewModel"
    }

    var loadClientsUseCase : LoadClientsUseCase? = null
    @Inject set

    private val mClientsLiveData : MutableLiveData<List<Client>> = MutableLiveData()
    private val mShowLoading : MutableLiveData<Boolean> = MutableLiveData()
    private val mShowError : MutableLiveData<Boolean> = MutableLiveData()

    private val mCompositeDisposable : CompositeDisposable = CompositeDisposable()

    init {
        Log.d(TAG, "init")
        (app as App).getAppComponent().inject(this)
        mClientsLiveData.value = null
        loadClients()
    }

    private fun loadClients() {
        mShowLoading.postValue(true)
        if (loadClientsUseCase == null) {
            mShowError.postValue(true)
            return
        }

        val disposable : Disposable = loadClientsUseCase!!.getClients()
                .subscribe(
                        {
                            clients : List<Client> ->
                            mClientsLiveData.postValue(clients)
                            mShowError.postValue(false)
                            mShowLoading.postValue(false)
                        },
                        {
                            throwable ->
                            Log.e(TAG, throwable.toString())
                            mClientsLiveData.postValue(null)
                            mShowLoading.postValue(false)
                            mShowError.postValue(true)
                        }
                )
        mCompositeDisposable.add(disposable)
    }

    fun getClients() : LiveData<List<Client>> {
        return mClientsLiveData
    }

    fun getShowLoading() : LiveData<Boolean> {
        return mShowLoading
    }

    fun getShowError() : LiveData<Boolean> {
        return mShowError
    }

    override fun onCleared() {
        mCompositeDisposable.dispose()
    }

    fun onUpdateClients() {
        loadClientsUseCase?.updateClients()
    }

    fun addClients() {
        val clients = mutableListOf<Client>()

        for ( i in 1..10) {
            val clientId = UUID.randomUUID().toString()
            val clientName = "Client $i"
            val clientAddress = "Екатеринбург ул.Тургенева д.4 кв $i"
            val clientPhone = "+78005553535"
            val location = "56.841122, 60.614942"
            val client = Client(clientId, clientName, clientAddress, clientPhone, location)
            clients.add(client)
        }

        loadClientsUseCase?.insertOrUpdateClients(clients)

    }
}