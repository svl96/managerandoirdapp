package com.manager.android.managerapp.ui

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.manager.android.managerapp.R
import com.manager.android.managerapp.domain.Client

class ClientsRecyclerAdapter(val callback: Callback) :
        RecyclerView.Adapter<ClientsRecyclerAdapter.ViewHolder>() {

    private var mClients: List<Client>? = null

    interface Callback {
        fun onItemClick(clientId: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.client_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mClients?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val client: Client = getItem(position)!!

        holder?.id = client.getId()
        holder?.name?.text = client.getName()
        holder?.phone?.text = client.getPhone()
    }

    fun getItem(position: Int) : Client? {
        return mClients?.get(position)
    }

    fun setClients(clients: List<Client>?) {
        when {
            clients == null -> {
                notifyItemRangeRemoved(0, itemCount)
            }

            mClients == null -> {
                mClients = clients
                notifyItemRangeInserted(0, clients.size)
            }
            else -> {
                val oldClients = mClients!!
                var res: DiffUtil.DiffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                    override fun getOldListSize(): Int {
                        return oldClients.size
                    }

                    override fun getNewListSize(): Int {
                        return clients.size
                    }

                    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int):
                            Boolean {
                        return oldClients[oldItemPosition].getId() ==
                                clients[newItemPosition].getId()
                    }

                    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                        val oldClient = oldClients[oldItemPosition]
                        val newClient = clients[newItemPosition]

                        return newClient.getId() == oldClient.getId() &&
                                TextUtils.equals(oldClient.getName(), newClient.getName()) &&
                                TextUtils.equals(oldClient.getPhone(), newClient.getPhone() )
                    }
                })
                mClients = clients
                res.dispatchUpdatesTo(this)
            }
        }
    }

    inner class ViewHolder(itemView: View?)
        : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var id: String = ""
        val name: TextView? = itemView?.findViewById(R.id.client_name)
        val phone: TextView? = itemView?.findViewById(R.id.client_phone)

        init {
            itemView?.setOnClickListener { v -> onClick(v) }
        }

        override fun onClick(p0: View?) {
            callback.onItemClick(id)
        }
    }


}