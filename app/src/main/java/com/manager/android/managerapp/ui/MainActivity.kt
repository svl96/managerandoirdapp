package com.manager.android.managerapp.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.manager.android.managerapp.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (supportFragmentManager.findFragmentByTag(ClientsFragment.TAG) == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_container,
                            ClientsFragment.getInstance(),
                            ClientsFragment.TAG
                            )
                    .commit()
        }
    }
}