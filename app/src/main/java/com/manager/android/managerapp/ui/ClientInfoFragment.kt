package com.manager.android.managerapp.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.manager.android.managerapp.R

class ClientInfoFragment : Fragment() {

    companion object {
        const val TAG = "ClientInfoFragment"
        const val CLIENT_ID_KEY = "CLIENT_ID"

        @JvmStatic
        fun getInstance(clientId: String) : ClientInfoFragment {
            val args = Bundle()
            args.putString(CLIENT_ID_KEY, clientId)

            val outFragment = ClientInfoFragment()
            outFragment.arguments = args
            return outFragment
        }
    }

    private var clientInfoViewModel : ClientInfoViewModel? = null

    private var nameView: TextView? = null
    private var addressView: TextView? = null
    private var phoneView: TextView? = null
    private var latlngView: TextView? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_client_info, container, false)!!
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        nameView = view?.findViewById(R.id.info_name)
        addressView = view?.findViewById(R.id.info_address)
        phoneView = view?.findViewById(R.id.info_phone)
        latlngView = view?.findViewById(R.id.info_latlng)

        addressView?.setOnClickListener {
            openAddress()
        }
    }

    private fun openAddress() {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container,
                        MapsFragment.getInstance(latlngView?.text as String),
                        MapsFragment.TAG)
                .addToBackStack(null)
                .commit()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        clientInfoViewModel = ViewModelProviders.of(this).get(ClientInfoViewModel::class.java)

        val clientId: String? = arguments.getString(CLIENT_ID_KEY)
        clientInfoViewModel?.loadClient(clientId)
        subscribeUI()
    }

    private fun subscribeUI() {

        clientInfoViewModel?.getNote()?.observe( this, Observer {
            client ->
            nameView?.text = client?.getName()
            addressView?.text = client?.getAddress()
            phoneView?.text = client?.getPhone()
            latlngView?.text = client?.getLocation()
            Log.d(TAG, client?.getLocation())
        })

        clientInfoViewModel?.getShowError()?.observe(this, Observer {
            isShowError ->
            if (isShowError != null && isShowError) {
                Toast.makeText(context, "Loading Error", Toast.LENGTH_SHORT).show()
            }
        }
        )
    }


}