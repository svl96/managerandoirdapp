package com.manager.android.managerapp.ui

import android.Manifest
import android.app.Application
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import com.manager.android.managerapp.R
import com.manager.android.managerapp.domain.route.RouteResponse
import com.google.android.gms.maps.model.LatLng
import android.location.Criteria
import android.content.Context.LOCATION_SERVICE
import android.location.Location
import android.location.LocationManager
import android.widget.Toast


class MapsFragment : Fragment(), OnMapReadyCallback {

    companion object {
        const val TAG = "MapsFragment"
        const val LOCATION_KEY = "LOCATION_KEY"

        @JvmStatic
        fun getInstance(location: String) : MapsFragment {
            val args = Bundle()
            args.putString(LOCATION_KEY, location)

            val outFragment = MapsFragment()
            outFragment.arguments = args
            return outFragment
        }
    }

    private lateinit var latlng: String
    private lateinit var mMap: GoogleMap
    private var mapsViewModel: MapsViewModel? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater?.inflate(R.layout.activity_maps, container, false)!!
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        latlng = arguments.getString(LOCATION_KEY)

        val mapFragment = childFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mapsViewModel = ViewModelProviders.of(this).get(MapsViewModel::class.java)

    }

    private fun drawRoute(routeResponse: RouteResponse?) {
        if (routeResponse == null) {
            return
        }

        val line = PolylineOptions()

        val color = Color.rgb(255, 0,0 )
        line.width(6f).color(color)
        val latLngBuilder = LatLngBounds.Builder()

        val points : List<LatLng> = PolyUtil.decode(routeResponse
                .getRoutes()[0].getOverviewPolyline().getPoints())

        for (point in points) {
            line.add(point)
            latLngBuilder.include(point)
        }

        mMap.addPolyline(line)
    }

    private fun subscribeUI() {
        mapsViewModel?.getRoute()?.observe(this, Observer {
            routeResponse: RouteResponse? ->
            drawRoute(routeResponse)
        })

        mapsViewModel?.getShowError()?.observe(this, Observer {
            isError ->
            if (isError != null && isError) {
                Toast.makeText(context, "Loading Error", Toast.LENGTH_SHORT).show()
            }
        })

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        Log.d(TAG, latlng)
        val lnglatarr = latlng.split(",")
        Log.d(TAG, lnglatarr.toString())
        val ekb = LatLng(lnglatarr[0].toDouble(), lnglatarr[1].toDouble())
        mMap.addMarker(MarkerOptions().position(ekb).title("Marker in Ekb"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ekb, 13.0f))

        var position = "56.848037, 60.661738"
        val destination = latlng

        if (ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true
            val service = context.getSystemService(LOCATION_SERVICE) as LocationManager
            var location : Location? = service.getLastKnownLocation(LocationManager.GPS_PROVIDER)

            if (location == null) {
                val criteria = Criteria()
                criteria.accuracy = Criteria.ACCURACY_COARSE
                val provider = service.getBestProvider(criteria, true)
                location = service.getLastKnownLocation(provider)

            }
            Log.d(TAG, "${location?.latitude}, ${location?.longitude}")

            if (location != null) {
                position = "${location.latitude}, ${location.longitude}"
            }
        }

        mapsViewModel?.loadRoute(position, destination)

        subscribeUI()

    }

}