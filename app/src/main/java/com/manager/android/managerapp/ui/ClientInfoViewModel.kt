package com.manager.android.managerapp.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.manager.android.managerapp.App
import com.manager.android.managerapp.domain.Client
import com.manager.android.managerapp.domain.LoadClientsUseCase
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.nio.BufferUnderflowException
import javax.inject.Inject


class ClientInfoViewModel(app: Application) : AndroidViewModel(app) {

    companion object {
        const val TAG = "ClientInfoViewModel"
    }

    init {
        (app as App).getAppComponent().inject(this)
    }

    var loadClientsUseCase : LoadClientsUseCase? = null
    @Inject set

    private val mShowError : MutableLiveData<Boolean> = MutableLiveData()
    private val mClientInfoLiveData : MutableLiveData<Client> = MutableLiveData()

    private val mCompositeDisposable = CompositeDisposable()

    fun loadClient(clientId: String?) {
        if (clientId.isNullOrEmpty()) {
            return
        }

        if (loadClientsUseCase == null) {
            mShowError.postValue(true)
            return
        }

        val disposable: Disposable = loadClientsUseCase!!.getClient(clientId!!)
                .subscribe(
                        { clients ->
                            if (clients.isNotEmpty()) {
                                val client = clients[0]
                                mClientInfoLiveData.postValue(client)
                                mShowError.postValue(false)
                            } else {
                                mShowError.postValue(true)
                                mClientInfoLiveData.postValue(null)
                            }
                        },
                        { throwable ->
                            Log.e(TAG, throwable.message)
                            mShowError.postValue(true)


                        }
                )

        mCompositeDisposable.add(disposable)
    }

    fun getNote() : LiveData<Client> {
        return mClientInfoLiveData
    }

    fun getShowError() : LiveData<Boolean>{
        return mShowError
    }

    override fun onCleared() {
        mCompositeDisposable.dispose()
    }

}