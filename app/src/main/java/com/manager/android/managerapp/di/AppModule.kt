package com.manager.android.managerapp.di

import android.arch.persistence.room.Room
import android.content.Context
import com.manager.android.managerapp.data.local.ClientsDao
import com.manager.android.managerapp.data.local.ClientsDatabase
import com.manager.android.managerapp.data.local.LocalRepositoryImpl
import com.manager.android.managerapp.data.remote.ClientsService
import com.manager.android.managerapp.data.remote.GoogleServicesImpl
import com.manager.android.managerapp.data.remote.RemoteRepositoryImpl
import com.manager.android.managerapp.data.remote.RouteService
import com.manager.android.managerapp.domain.GoogleServices
import com.manager.android.managerapp.domain.LoadClientsUseCase
import com.manager.android.managerapp.domain.LocalRepository
import com.manager.android.managerapp.domain.RemoteRepository
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule(private val appContext: Context) {

    @Provides
    fun context(): Context {
        return appContext
    }

    @Provides
    @Singleton
    fun provideClientsDatabase() : ClientsDatabase {
        return Room.databaseBuilder(
                appContext,
                ClientsDatabase::class.java,
                ClientsDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideClientsDao(clientsDatabase: ClientsDatabase) : ClientsDao {
        return clientsDatabase.clientsDao()
    }

    @Provides
    @Singleton
    fun provideLoadClientsUseCase(localRepository: LocalRepository,
                                  remoteRepository: RemoteRepository,
                                  googleServices: GoogleServices) : LoadClientsUseCase {
        return LoadClientsUseCase(localRepository, remoteRepository, googleServices)
    }

    @Provides
    @Singleton
    fun provideLocalRepository(clientsDao: ClientsDao) : LocalRepository {
        return LocalRepositoryImpl(clientsDao)
    }

    @Provides
    @Singleton
    fun provideRemoteRepository(clientsService: ClientsService) : RemoteRepository {
        return RemoteRepositoryImpl(clientsService)
    }

    @Provides
    @Singleton
    fun provideClientService() : ClientsService {
        return object : ClientsService {}
    }

    @Provides
    @Singleton
    fun provideRouteService() : RouteService {
        return Retrofit.Builder()
                .baseUrl(RouteService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(RouteService::class.java)
    }

    @Provides
    @Singleton
    fun provideGoogleServices(routeService: RouteService) : GoogleServices {
        return GoogleServicesImpl(routeService)
    }



}