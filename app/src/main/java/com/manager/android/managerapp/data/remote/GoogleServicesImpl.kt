package com.manager.android.managerapp.data.remote

import android.util.Log
import com.manager.android.managerapp.domain.GoogleServices
import com.manager.android.managerapp.domain.route.RouteResponse
import io.reactivex.Single
import retrofit2.Response


class GoogleServicesImpl(private val routeService: RouteService) : GoogleServices {

    private val sensor: Boolean = true
    private val language: String = "ru"

    override fun getRoute(position: String, destination: String): Single<Response<RouteResponse>> {
        Log.d("GoogleService", "LoadRoute")
        return routeService.getRoute(position, destination, sensor, language)
    }
}