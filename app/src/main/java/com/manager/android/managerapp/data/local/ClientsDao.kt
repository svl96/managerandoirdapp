package com.manager.android.managerapp.data.local

import android.arch.persistence.room.*
import com.manager.android.managerapp.domain.Client
import io.reactivex.Flowable
import org.jetbrains.annotations.NotNull


@Dao
abstract class ClientsDao {
    companion object {
        const val TABLE_NAME = "clients"
    }

    interface Columns {
        companion object {
            const val ID = "id"
            const val NAME = "name"
            const val ADDRESS = "address"
            const val PHONE = "phone"
            const val LOCATION = "location"
        }
    }

    @NotNull
    @Query("SELECT * FROM $TABLE_NAME")
    abstract fun getClientList() : Flowable<List<Client>>

    @NotNull
    @Query("SELECT * FROM $TABLE_NAME WHERE id=:uid")
    abstract fun getClient(uid: String) : Flowable<List<Client>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrUpdate(clients: List<Client>)

    @Delete
    abstract fun deleteClient(client: Client)

}